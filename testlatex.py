import unittest

import latex

class EnvironmentClassTest(unittest.TestCase):
    """
    Test Environment class.
    """
    environments = { 
        "test":"\\begin{test}\nTo je test.\n\\end{test}",
        "btest":"\\begin{btest}[btest opt]{btest arg}\nTo je btest. \n\n\\end{btest}",
        }

    def testInit(self):
        """test constructor"""
        result = latex.Environment("test")
        self.assertEqual(result.name,"test")


if __name__ == '__main__':
        unittest.main()
