import re

class Environment:
    """LaTeX environment"""
    def __init__(self,name):
        self.name = name
        self.regex = re.compile(r"\\begin\{" + self.name + r"\}(\{(.*?)\}){0,1}(.*?)\\end\{" + self.name + r"\}", re.DOTALL)

